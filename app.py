from hailstorm import hailstorm

n = int(input("Enter your number :"))
hailstorm_list = hailstorm(n)
print(f"The hailstorm sequence for n = {n} is : {hailstorm_list}")

