hailstorm_list = []
def hailstorm(n):
    hailstorm_list.append(int(n))
    if n == 1:
        return hailstorm_list
    while n != 1:
        if n % 2 == 0:
            return hailstorm(int(n/2))
        else:
            return hailstorm(int(n*3 + 1))
